import Ember from 'ember';

export default Ember.Service.extend({
  init() {
    this._super(...arguments);
    this._photoCache = {};
  },

  reset() {
    this._photoCache = {};
  },

  search(searchTerm) {
    return this.apiCall('flickr.photos.search', {tags: searchTerm}).then(res => {
      let photos = res.photos.photo;
      photos.forEach(photo => {
        this._photoCache[photo.id] = photo;
      });
      return photos;
    });
  },
  findPhoto(photoId) {
    let cachedPhoto = this._photoCache[photoId];
    if (cachedPhoto) {
      return Ember.RSVP.resolve(cachedPhoto);
    } else {
      return this.apiCall('flickr.photos.getInfo', {photo_id: photoId}).then(res => { return res.photo; });
    }
  },
  apiCall(method, params) {
    let url = `https://api.flickr.com/services/rest/?method=${method}&api_key=c0e2dcf2a49e24683baf7ef09710f95b&format=json&nojsoncallback=1`;
    url += '&' + $.param(params);
    return Ember.RSVP.resolve($.getJSON(url));
  }
});
