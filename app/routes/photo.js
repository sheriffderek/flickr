import Ember from 'ember';
import $ from 'jquery';

export default Ember.Route.extend({
  flickr: Ember.inject.service(),

  model(params) {
    return this.get('flickr').findPhoto(params.photoId);
  }
});
